package com.artifact.utility;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class DataBaseConnection {
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/artifact", "root",
					"");
			if (con == null)

			{
				throw new IllegalArgumentException(
						"Connection could not be established please contact System Administrator");

			}
			return con;
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("Drivers Not found");
		} catch (SQLException e) {
			throw new IllegalArgumentException("Please Check The Database connection");
		}

	}

	public static void main(String[] args) {
		System.out.println(DataBaseConnection.getConnection());
	}

}
