package com.artifact.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.artifact.entity.MyEntity;

public class ArtifactDaoImpl implements ArtifactDao {
	Session session = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
	Transaction t = session.beginTransaction();

	public void insertRecords(MyEntity enobj) {

		session.persist(enobj);
		t.commit();
		session.close();

	}

	public void updateRecord(MyEntity enobj) {
		String queryString = "from emp1000 where id = :id";
        Query query = session.createQuery(queryString);
        query.setInteger("id", enobj.getId());
        MyEntity employee=(MyEntity) query.uniqueResult();
        session.delete(employee);
        System.out.println("One employee is deleted!");

	}

	public static void main(String[] args) {
		MyEntity enonj = new MyEntity();
		enonj.setAddress("sagara");
		enonj.setName("ram");
		enonj.setId(11);
		ArtifactDaoImpl im = new ArtifactDaoImpl();
		// im.insertRecords(enonj);
		im.updateRecord(enonj);

	}

}
